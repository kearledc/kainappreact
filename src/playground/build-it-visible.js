let visibility = false;

const buttonClick = () => {
	visibility = !visibility;
	renderToggleApp();
}




const app = document.getElementById('app');

const renderToggleApp = () => {
	
	const toggleApp = (
		<div>
			<h1>Visibility Toggle</h1>
			<button onClick={buttonClick}>{visibility ? "Hide Details":"Show Details"}</button>
			{visibility ? <p>lorem ipsum</p>:""}
		</div>
	)

	ReactDOM.render(toggleApp, app)
}

renderToggleApp();