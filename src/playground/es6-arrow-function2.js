const add = (a, b) => {
	return a+b;
}


// console.log(add(5,3));

const user = {
	name : 'Earle',
	cities : ['QC', 'Pasig', 'Manila'],
	printPlacesLived(){
		return this.cities.map((city) => this.name + ' has lived in ' + city);

		// this.cities.forEach((city) => {
		// 	console.log(this.name + ' has lived in ' + city);
		// })
	}
};

// console.log(user.printPlacesLived());


const multiplier = {
	numbers : ['3', '6', '9'],
	multiplyBy : 2,
	multiply(){
		return this.numbers.map((number) => number * this.multiplyBy);
	}
}

console.log(multiplier.multiply());