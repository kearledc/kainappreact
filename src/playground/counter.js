
let count = 0;

const addOne = () => {
	count++;
	renderCounterApp();	
};

const minusOne = () => {
	count--;
	renderCounterApp();
}

const reset = () => {
	// console.log('reset')
	count = 0;
	renderCounterApp();
}



// console.log(templateTwo)





const renderCounterApp = () => {
	
	const templateTwo = (
		<div>
			<h1>Count: {count}</h1>
				<button onClick={addOne}>+1</button>
				<button onClick={minusOne}>-1</button>
				<button onClick={reset}>Reset</button>
		</div>
		)

	ReactDOM.render(templateTwo , appRoot);

}

renderCounterApp();