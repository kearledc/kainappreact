// JSX - JavaScript XML

const app = {
	title: 'Saan tayo kakain?',
	subtitle: 'Ask me',
	options: []
}


const onFormSubmit = (e) => {
	e.preventDefault();

	const option = e.target.elements.option.value;

	if(option){
		app.options.push(option);
		e.target.elements.option.value = ' ';
		renderFormApp();
	}


};

const resetOptions = () => {
	app.options.length = 0;
	renderFormApp();
	console.log('hello from reset');
}


let appRoot = document.getElementById('app');

const onMakeDecision = () => {
	const randomNum = Math.floor(Math.random() * app.options.length);
	const option = app.options[randomNum];
	// console.log(option);
	alert(option);
};


const numbers = [55, 101, 1000];

const renderFormApp = () => {
	const template = (
		<div>
		
			<h1>{app.title}</h1>
			<div className="line"></div>
			{app.subtitle && <h2>{app.subtitle}</h2>}


		
			<div className="container">
				<button className="buttons" disabled={app.options.length == 0}onClick={onMakeDecision}>Saan kaba kakain?</button>
				<button className="buttons" onClick={resetOptions}>Reset</button>
			</div>
				
				<form onSubmit={onFormSubmit}>
					<input type="text" name="option"/>
					<button className="buttons2">Add Kainan</button>
				</form>
			<ol>
			{
				app.options.map((option) => {
					return <li key={option}>Option: {option}</li>;
				})
			}
			</ol>

			{/* <ul>
							<li>1</li>
							<li>2</li>
						</ul> */} 
		</div>
	);

	ReactDOM.render(template, appRoot);
}

renderFormApp();