'use strict';

// JSX - JavaScript XML

var app = {
	title: 'Saan tayo kakain?',
	subtitle: 'Ask me',
	options: []
};

var onFormSubmit = function onFormSubmit(e) {
	e.preventDefault();

	var option = e.target.elements.option.value;

	if (option) {
		app.options.push(option);
		e.target.elements.option.value = ' ';
		renderFormApp();
	}
};

var resetOptions = function resetOptions() {
	app.options.length = 0;
	renderFormApp();
	console.log('hello from reset');
};

var appRoot = document.getElementById('app');

var onMakeDecision = function onMakeDecision() {
	var randomNum = Math.floor(Math.random() * app.options.length);
	var option = app.options[randomNum];
	// console.log(option);
	alert(option);
};

var numbers = [55, 101, 1000];

var renderFormApp = function renderFormApp() {
	var template = React.createElement(
		'div',
		null,
		React.createElement(
			'h1',
			null,
			app.title
		),
		React.createElement('div', { className: 'line' }),
		app.subtitle && React.createElement(
			'h2',
			null,
			app.subtitle
		),
		React.createElement(
			'div',
			{ className: 'container' },
			React.createElement(
				'button',
				{ className: 'buttons', disabled: app.options.length == 0, onClick: onMakeDecision },
				'Saan kaba kakain?'
			),
			React.createElement(
				'button',
				{ className: 'buttons', onClick: resetOptions },
				'Reset'
			)
		),
		React.createElement(
			'form',
			{ onSubmit: onFormSubmit },
			React.createElement('input', { type: 'text', name: 'option' }),
			React.createElement(
				'button',
				{ className: 'buttons2' },
				'Add Kainan'
			)
		),
		React.createElement(
			'ol',
			null,
			app.options.map(function (option) {
				return React.createElement(
					'li',
					{ key: option },
					'Option: ',
					option
				);
			})
		)
	);

	ReactDOM.render(template, appRoot);
};

renderFormApp();
